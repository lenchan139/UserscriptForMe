// ==UserScript==
// @name         Yande.re resize auto clicker
// @namespace    https://tto.moe
// @version      0.1
// @description  Resize the image to maximum! Yeah!
// @author       Len Chan
// @match        https://yande.re/post/show/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    let resize_notice = document.getElementById('resized_notice')
    if(resize_notice){
        let resize_click = resize_notice.getElementsByClassName('highres-show')
        if(resize_click && resize_click.length > 0){
            resize_click[0].click()
        }
    }
})();