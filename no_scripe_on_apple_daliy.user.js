// ==UserScript==
// @name         Hide Subscription MessageBox on AppleDaliy
// @namespace    http://tto.moe/
// @version      0.1
// @description  hide it! Now!
// @author       Len Chan
// @match        https://*.appledaily.com/*
// @match        https://hk.*.appledaily.com/*
// @match        https://tw.*.appledaily.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    articleContent.outerHTML = "<div>" + articleContent.innerHTML + "</div>";
    //$('#articleContent').after("<p>" + articleContent.innerHTML + "</p>");
    //articleContent.style.display = 'none';
    // Your code here...
})();