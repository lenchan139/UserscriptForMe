// ==UserScript==
// @name         Click twitter to top
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  click twitter icon to top!
// @author       Len Chan
// @match        https://twitter.com/*
// @grant        none
// ==/UserScript==

var shouldInit = true;
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function loadToTop() {
    document.querySelector('#react-root > div > div > div > main > div > div.css-1dbjc4n.r-aqfbo4.r-1niwhzg.r-16y2uox > div > div > div > div > div.css-1dbjc4n.r-aqfbo4.r-14lw9ot.r-my5ep6.r-rull8r.r-qklmqi.r-gtdqiz.r-ipm5af.r-1g40b8q > div.css-1dbjc4n.r-1p0dtai.r-18u37iz.r-1777fci.r-1d2f490.r-12vffkv.r-u8s1d.r-zchlnj.r-1ej1qmr.r-184en5c > div > div').click()
}

function loadTwiceTop() {
    loadToTop();
    setTimeout(() => {
        loadToTop();
        setTimeout(() => {
            loadToTop();
        }, 1000)
    }, 300)
}

function initTopBotton() {

    console.log('y')
    // Your code here...
    var obj = document.querySelector('#react-root > div > div > div > header > div > div > div > div > div.css-1dbjc4n.r-dnmrzs.r-1vvnge1 > h1 > a');
    if (obj) {
        obj
            .addEventListener('click', function () {
                console.log("clicked")
                loadTwiceTop();
            })
        shouldInit = false;
    }
    console.log('y')

}
async function initButton() {
    while (shouldInit) {
        initTopBotton()
        await sleep(1500)
    }
}
(function () {
    'use strict';
    initButton();
})();