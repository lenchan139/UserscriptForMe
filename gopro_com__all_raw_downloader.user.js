// ==UserScript==
// @name         gopro.com Media Libary All RAW Downloader
// @namespace    https://tto.moe
// @version      1.1
// @description  download all selected RAW(if possible) or JPEG from gopro.com media libary.
// @author       lenchan139
// @match        https://plus.gopro.com/media-library/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gopro.com
// @grant        none
// ==/UserScript==

(function () {
    'use strict';

    // Your code here...
    async function downloadMultiOnGoProMediaLibary() {
        const count = prompt("how many next media should download?")
        for (let i = 0; i < count; i++) {

            console.log(`handling ${i}/${count}`)
            await tryDownloadSingleMedia()
        }
        alert('finished!')
    }
    async function tryDownloadSingleMedia(isRaw) {

        try {
            let thatId = "JPG Image"
            if(isRaw){
                thatId = "RAW Source"

            }

            document.getElementById(thatId).click();
        } catch (e) {
            document.querySelector('#media-library > main > div > div.media-viewer.active.no-transition > div.viewer.active > div.toolbar > div.social-icons > div > ul > li.download-menu-item > a').click()
        }

        // document.querySelector('#media-library > main > div > div.media-viewer.active.no-transition > div.viewer.active > div.inner > button.icon-nav-bg-right').click()


    }
    async function awaitDetailClose() {
        while (document.querySelector('#media-library > main > div > div.media-viewer.active.no-transition > div.nav-bar > button') != null) {
            console.log('waiting close...')
            await sleep(1000)
        }
    }

    async function awaitDetailOpen() {
        while (document.querySelector('#media-library > main > div > div.media-viewer.active.no-transition > div.nav-bar > button') == null) {
            console.log('waiting open...')
            await sleep(1000)
        }
    }
    async function sleep(c) {
        return new Promise((resolved, reject) => {
            setTimeout(() => {
                resolved(true)
            }, c);
        })
    }
    function appendMultiLoginButton() {

        const uniqueRawId = 'unique_multiDownload_00f_button_raw'
        const uniqueJpegId = 'unique_multiDownload_00f_button_jpeg'
        const P = document.getElementById(uniqueRawId)
        if (P) {
            P.outerHTML = ''
        }

        const Z = document.getElementById(uniqueJpegId)
        if(Z){
            Z.outerHTML = ''
        }
        const downloadButton = document.querySelector('#media-library > main > div > div.index-header.select-mode > div.selecting-right-header > button.btn.header-btn.selection-download-button')
        if (downloadButton && downloadButton.parentNode) {
            const R = document.createElement('button')
            const J = document.createElement('button')

            R.innerHTML = `<i class="icon icon-chevron-down"></i> RAW`
            R.id = uniqueRawId
            R.type = 'button'
            R.className = "btn header-btn selection-download-button"
            R.onclick = () => {
                clickCountAndSelectedElementIds(true)
            }

            J.innerHTML = `<i class="icon icon-chevron-down"></i> JPG`
            J.id = uniqueJpegId
            J.type = 'button'
            J.className = "btn header-btn selection-download-button"
            J.onclick = () => {
                clickCountAndSelectedElementIds(false)
            }

            downloadButton.parentNode.prepend(J)
            downloadButton.parentNode.prepend(R)
        }
    }
    function closeSelectedPopup() {
        document.querySelector('#media-library > main > div > div.index-header.select-mode > div.selection-section > div > button').click()
        console.log('ok')
    }

    function closeOpenedPopup() {
        document.querySelector('#media-library > main > div > div.media-viewer.active.no-transition > div.nav-bar > button').click()
    }
    async function clickCountAndSelectedElementIds(isRaw) {
        const arrSelectedElement = document.querySelectorAll('div > div.overlay-selection.show-overlay.selected')
        let itemList = []
        const index = 1
        for (const obj of arrSelectedElement) {
            const id = await getSigleMediaId(obj)
            console.log(`(${index}/${arrSelectedElement.length}) SUCC ${id}`)
            if (id) {
                itemList.push(id)
            }
            i++
        }
        console.log(itemList)
        await closeSelectedPopup()

        sleep(10000)
        for (const mediaId of itemList) {
            await awaitDetailClose()
            await sleep(150)
            await clickMediaDetail(mediaId)
            await sleep(150)
            await tryDownloadSingleMedia(isRaw)
            await sleep(150)
            await closeOpenedPopup()
            await sleep(300)
            await awaitDetailClose()
            await sleep(100)
        }
        alert('all download completed!')
    }

    async function clickMediaDetail(mediaId) {

        const mediaNode = document.getElementById(mediaId)
        console.log('try click ' + mediaId, mediaId)
        if (mediaNode) {
            const clickNode = mediaNode.getElementsByTagName('a')
            if (clickNode && clickNode.length > 0 && clickNode[0]) {
                console.log(mediaId)
                clickNode[0].click()
                await awaitDetailOpen()
                // await tryDownloadSingleMedia()
                return true
            }
        }

    }
    async function getSigleMediaId(obj) {
        if (obj?.parentElement?.parentElement?.id) {
            const mediaId = obj.parentElement.parentElement?.id
            const mediaNode = document.getElementById(mediaId)
            if (mediaNode) {
                // const clickNode = mediaNode.getElementsByTagName('a')
                // clickNode.click()
                // await tryDownloadSingleMedia()
                return mediaId
            }
        }
    }
    function appendMultiLoginButtonOld() {

        document.querySelectorAll('div > div.overlay-selection.show-overlay.selected')
        const btn = document.createElement('button')
        btn.innerText = "DL M"
        btn.style.color = 'black'
        btn.onclick = downloadMultiOnGoProMediaLibary
        document.querySelector('#media-library > main > div > div.media-viewer.active.no-transition > div.viewer.active > div.toolbar > div.toolbar-player-edits').append(btn)
    }

    setInterval(() => {

        appendMultiLoginButton();
    }, 1000)
})();
