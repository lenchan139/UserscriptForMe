// ==UserScript==
// @name         Auto to Raw for Google+ Image
// @namespace    http://lenchan139.org/
// @version      1.3
// @description  Auto to raw for Google+ Images.
// @author       lenchan139
// @match        https://*.googleusercontent.com/*
// @grant        none
// ==/UserScript==
(function() {
    'use strict';

    var short = window.location.href.search(/\/\w\d+(-\w\d*)-rw\//);
    var long = window.location.href.search(/\/\w\d+(-\w\d*)-[a-zA-Z]-rw\//);
    var mid = window.location.href.search(/\/\w\d+(-\w\d*)-[a-zA-Z]\//);

    var header = window.location.href.search(/https:\/\/lh[0-9]+.googleusercontent.com/g);

    var newUrl =  window.location.href;

    if (short != -1 && header != -1) {
        newUrl = window.location.href.replace(/\/\w\d+(-\w\d*)-rw\//, '/s0/');
        window.location.replace(newUrl);
    } else if (long != -1 && header != -1) {
        newUrl = window.location.href.replace(/\/\w\d+(-\w\d*)-[a-zA-Z]-rw\//, '/s0/');
        window.location.replace(newUrl);
    } else if (mid != -1 && header != -1) {
        newUrl = window.location.href.replace(/\/\w\d+(-\w\d*)-[a-zA-Z]\//, '/s0/');
        window.location.replace(newUrl);
    }
})();