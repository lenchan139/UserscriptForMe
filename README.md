# UserscriptForMe
There are some userscript for self-use.
### Licenses
GPLv3


# Scripts
### gopro_com__all_raw_downloader.user.js
since there no download raw option when multi-selected download in default, and gopro.com limit bulk-download 25 per time, so I create this script that can simulate "open image, download raw, then close, then open next image, download raw ..." that loop. 
Remember, that will download each spilt JPEG / RAW in. you better set your auto select download location. nor you will get very annoying.

![](images/Screenshot_20220515_083832.png)


Once you install, then select any, you will saw that button, just press it then it starts.
